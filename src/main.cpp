#include <windows.h>

#include "TSFuncs.hpp"

enum CLASS_TYPES
{
	CLASS_NetObject,
	CLASS_GameBase,
	CLASS_ShapeBase,
	CLASS_Projectile,
	CLASS_PhysicalZone,
	CLASS_fxDTSBrick,
};

BlFunctionDef(float, __thiscall, NetObject__getUpdatePriority, ADDR, ADDR, unsigned int, signed int);
BlFunctionHookDef(NetObject__getUpdatePriority);

BlFunctionDef(float, __thiscall, GameBase__getUpdatePriority, ADDR, ADDR, unsigned int, signed int);
BlFunctionHookDef(GameBase__getUpdatePriority);

BlFunctionDef(float, __thiscall, ShapeBase__getUpdatePriority, ADDR, ADDR, unsigned int, signed int);
BlFunctionHookDef(ShapeBase__getUpdatePriority);

BlFunctionDef(float, __thiscall, Projectile__getUpdatePriority, ADDR, ADDR, unsigned int, signed int);
BlFunctionHookDef(Projectile__getUpdatePriority);

BlFunctionDef(float, __thiscall, PhysicalZone__getUpdatePriority, ADDR, ADDR, unsigned int, signed int);
BlFunctionHookDef(PhysicalZone__getUpdatePriority);

BlFunctionDef(float, __thiscall, fxDTSBrick__getUpdatePriority, ADDR, ADDR, unsigned int, signed int);
BlFunctionHookDef(fxDTSBrick__getUpdatePriority);

float Generic_getUpdatePriority(ADDR obj, ADDR camScopeQuery,
                unsigned int updateMask, signed int updateSkips,
                CLASS_TYPES CLASS_TYPE)
{
	const char *priority = tsf_GetDataField(obj, "networkPriority", NULL);
	if (priority != NULL && *priority != '\0')
		return atof(priority);
	else {
		switch (CLASS_TYPE) {
			case CLASS_NetObject:
				return NetObject__getUpdatePriorityOriginal(obj, camScopeQuery, updateMask, updateSkips);
			case CLASS_GameBase:
				return GameBase__getUpdatePriorityOriginal(obj, camScopeQuery, updateMask, updateSkips);
			case CLASS_ShapeBase:
				return ShapeBase__getUpdatePriorityOriginal(obj, camScopeQuery, updateMask, updateSkips);
			case CLASS_Projectile:
				return Projectile__getUpdatePriorityOriginal(obj, camScopeQuery, updateMask, updateSkips);
			case CLASS_PhysicalZone:
				return PhysicalZone__getUpdatePriorityOriginal(obj, camScopeQuery, updateMask, updateSkips);
			case CLASS_fxDTSBrick:
				return fxDTSBrick__getUpdatePriorityOriginal(obj, camScopeQuery, updateMask, updateSkips);
			default:
				return 1000.f;
		}
	}
}

float __fastcall NetObject__getUpdatePriorityHook(ADDR obj, void *blank, ADDR camScopeQuery, unsigned int updateMask, signed int updateSkips)
{
	return Generic_getUpdatePriority(obj, camScopeQuery, updateMask, updateSkips, CLASS_NetObject);
}

float __fastcall GameBase__getUpdatePriorityHook(ADDR obj, void *blank, ADDR camScopeQuery, unsigned int updateMask, signed int updateSkips)
{
	return Generic_getUpdatePriority(obj, camScopeQuery, updateMask, updateSkips, CLASS_GameBase);
}

float __fastcall ShapeBase__getUpdatePriorityHook(ADDR obj, void *blank, ADDR camScopeQuery, unsigned int updateMask, signed int updateSkips)
{
	return Generic_getUpdatePriority(obj, camScopeQuery, updateMask, updateSkips, CLASS_ShapeBase);
}

float __fastcall Projectile__getUpdatePriorityHook(ADDR obj, void *blank, ADDR camScopeQuery, unsigned int updateMask, signed int updateSkips)
{
	return Generic_getUpdatePriority(obj, camScopeQuery, updateMask, updateSkips, CLASS_Projectile);
}

float __fastcall PhysicalZone__getUpdatePriorityHook(ADDR obj, void *blank, ADDR camScopeQuery, unsigned int updateMask, signed int updateSkips)
{
	return Generic_getUpdatePriority(obj, camScopeQuery, updateMask, updateSkips, CLASS_PhysicalZone);
}

float __fastcall fxDTSBrick__getUpdatePriorityHook(ADDR obj, void *blank, ADDR camScopeQuery, unsigned int updateMask, signed int updateSkips)
{
	return Generic_getUpdatePriority(obj, camScopeQuery, updateMask, updateSkips, CLASS_fxDTSBrick);
}

bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	//0x59a950 NetObject::getUpdatePriority()
	BlScanFunctionHex(NetObject__getUpdatePriority, "DB 44 24 0C D9 5C 24 0C D9 44 24 0C DC 0D");
	BlCreateHook(NetObject__getUpdatePriority);
	BlTestEnableHook(NetObject__getUpdatePriority);

	//0x4b67d0 GameBase::getUpdatePriority()
	BlScanFunctionHex(GameBase__getUpdatePriority, "83 EC 4C 56 8B F1");
	BlCreateHook(GameBase__getUpdatePriority);
	BlTestEnableHook(GameBase__getUpdatePriority);

	//0x4f6fe0 ShapeBase::getUpdatePriority()
	BlScanFunctionHex(ShapeBase__getUpdatePriority, "8B 54 24 04 8B 02 3B C1");
	BlCreateHook(ShapeBase__getUpdatePriority);
	BlTestEnableHook(ShapeBase__getUpdatePriority);

	//0x4e99c0 Projectile::getUpdatePriority()
	BlScanFunctionHex(Projectile__getUpdatePriority, "56 8B 74 24 08 57 FF 74 24 14");
	BlCreateHook(Projectile__getUpdatePriority);
	BlTestEnableHook(Projectile__getUpdatePriority);

	//0x4d1820 PhysicalZone::getUpdatePriority()
	BlScanFunctionHex(PhysicalZone__getUpdatePriority, "D9 05 ? ? ? ? C2 0C 00");
	BlCreateHook(PhysicalZone__getUpdatePriority);
	BlTestEnableHook(PhysicalZone__getUpdatePriority);

	//0x492a80 fxDTSBrick::getUpdatePriority()
	BlScanFunctionHex(fxDTSBrick__getUpdatePriority, "51 56 8B F1 80 BE 47 03 00 00 00");
	BlCreateHook(fxDTSBrick__getUpdatePriority);
	BlTestEnableHook(fxDTSBrick__getUpdatePriority);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");

	return true;
}

bool deinit()
{
	BlTestDisableHook(NetObject__getUpdatePriority);
	BlTestDisableHook(GameBase__getUpdatePriority);
	BlTestDisableHook(ShapeBase__getUpdatePriority);
	BlTestDisableHook(Projectile__getUpdatePriority);
	BlTestDisableHook(PhysicalZone__getUpdatePriority);
	BlTestDisableHook(fxDTSBrick__getUpdatePriority);

	BlPrintf("%s: deinit'd", PROJECT_NAME);

	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
